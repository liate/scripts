#!/usr/bin/env bash
echo "Jukebox mode -- activating!"
x=`xbacklight`
killall xautolock
echo "
"
mpc play
sleep 5
if [ ! -n "$1" ]
then
	xbacklight = 0
else
	xbacklight = $1
fi
read -t 3600 -n 1 n
echo "$n"
xbacklight = $x
mpc pause
bgcmd xautolock -time 10 -locker blurlock
i3exit suspend
# echo "Not suspending now, bcs testing"
