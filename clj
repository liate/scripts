#!/usr/bin/env sh
breakchars="(){}[],^%$#@\"\";:''|\\"
CLOJURE_DIR=/usr/share/clojure-1.10/lib/
CLOJURE_JAR="$CLOJURE_DIR"/clojure.jar
if [ $# -eq 0 ]; then 
	exec rlwrap --remember -c -b "$breakchars" \
		-f "$HOME"/.clj_completions \
		clojure-1.10 "$@"
else
	exec java -cp "$CLOJURE_JAR" clojure.main $1 -- "$@"
fi
