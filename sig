#!/usr/bin/env bash
# Script to use fortune files as part of, but not all of, my
# email signature.
echo "Andrew Patterson"
echo 'From /usr/bin/fortune:
=='
# fortune -e $(cat ~/personal-fortune-files)
fortune
#Carthago delenda est
