#!/usr/bin/env bash

THETIME=1500000000

TIME=$(date +%s)
COUNT="$(($THETIME - $TIME))"

while [ $COUNT -ge 0 ]; do
    echo -ne "Current time: $TIME; $COUNT seconds until $THETIME seconds since epoch \033[0K\r"
    if [ $COUNT -eq 0 ]; then echo "HAPPY $(bc <<< "scale=1; $THETIME/1000000000") BILLION SECONDS SINCE EPOCH"'!!!'; fi
    sleep 1
    TIME=$(date +%s)
    COUNT="$(($THETIME - $TIME))"
done
