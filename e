#!/bin/sh

# Both shorter _and_ dynamically dispatches on if X is running
if [ $DISPLAY != "" ]; then
	emacsclient -a "" "$@"
else
	vim "$@"
fi
