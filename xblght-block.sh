#!/usr/bin/env zsh

if [[ -n $BLOCK_INSTANCE ]]; then
    password=("-h" "$BLOCK_INSTANCE@localhost")
fi

round() {
	printf %.0f $1
}

case $BLOCK_BUTTON in
	4) xbacklight = $(($(round $(xbacklight)) + 5));;
	5) xbacklight = $(($(round $(xbacklight)) - 5));;
esac

round $(xbacklight)
echo "%"
