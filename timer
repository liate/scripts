#!/usr/bin/env sbcl --script
; vim: set filetype=lisp:
(defun tick (secs)
  (sleep 1)
  (1- secs))

(defun min-sec-string (secs)
  (format nil "~3D:~2,'0D" (truncate secs 60) (mod secs 60)))

(defun restart-line ()
  (princ "[1000D[2K"))

(defun usage ()
  (format t "Usage: ~A <minutes> [seconds]~%" "timer"))

(defun my-timer (secs)
  (unless (= secs 0)
    (restart-line)
    (format t "~A left (~D s)" (min-sec-string secs) secs)
    (finish-output)
    (my-timer (tick secs))))

(handler-case
  (let ((len (length sb-ext:*posix-argv*))
        (args sb-ext:*posix-argv*))
    (cond ((= len 2)
           (my-timer (truncate (* (read-from-string (cadr args)) 60))))
          ((= len 3)
           (my-timer (truncate
                       (+ (* (read-from-string (cadr args)) 60)
                          (read-from-string (caddr args))))))
          (t (usage) (sb-ext:exit :abort t))))
  (sb-sys:interactive-interrupt () (format t "Timer aborted~%") (sb-ext:exit :abort t))
  (t (err) (format t "Caught error \"~A\"; aborting~%" err) (usage) (sb-ext:exit :abort t)))

(restart-line)
