(require 'lquery)
(require 'dexador)
(require 'quri)

(defun rss-find (url)
  (let ((doc (lquery:$ (initialize (dex:get url)))))
    (lquery:$
      doc
      "link[rel=alternate][type=application/rss+xml], link[rel=alternate][type=application/atom+xml]"
      (attr :href))))

(defun usage ()
  (format *error-output* "Usage: ~a <url>~%" (car *posix-argv*)))

(defun main ()
  (handler-case
      (if (> (length *posix-argv*) 1)
          (let ((url (nth 1 *posix-argv*)))
            (format t "~{~a~%~}"
                    (map 'list
                         (lambda (u)
                           (quri:render-uri (quri:merge-uris u url)))
                         (rss-find url))))
          (usage))
    (sb-sys:interactive-interrupt () (terpri) (sb-ext:exit :abort t))
    (t (err) (format *error-output* "Caught error \"~A\"; aborting~%" err) (sb-ext:exit :abort t))))

(defun make (&optional (fname "rss-find"))
  (sb-ext:save-lisp-and-die fname
                            :toplevel #'main
                            :executable t
                            :purify t
                            :compression t))
