#!/usr/bin/env bash
maxjobs=$(grep processor /proc/cpuinfo | wc -l)
midi2ogg() {
	name=$(echo $@ | sed -r s/[.][mM][iI][dD][iI]?$//g | sed s/^[.][/]//g)
	for arg; do 
		fluidsynth -nli -r 48000 -o synth.cpu-cores=$maxjobs -F "/dev/shm/$name.raw" \
				   /usr/share/soundfonts/FluidR3_GM.sf2 "$@"
		oggenc -r -B 16 -C 2 -R 48000 "/dev/shm/$name.raw" -o "$name.ogg"
		rm "/dev/shm/$name.raw"
		## Uncomment for replaygain tagging
		#vorbisgain -f "$name.ogg" 
	done
}
export -f midi2ogg
find . -regex '.*[.][mM][iI][dD][iI]?$' -print0 | xargs -0 -n 1 -P $maxjobs bash -c 'midi2ogg "$@"' --
