#!/usr/bin/env bash 
# From <https://shadowkat.net/news/21.html>
if [ -z "$3" ]; then 
bitrate="1M" 
else 
bitrate="$3" 
fi 
echo Encoding at a bitrate of $bitrate 
ffmpeg -y -i "$1" -c:v libvpx-vp9 -vf scale=-1:720 -pass 1 -qmin 0 -qmax 50 -crf 10 -b:v "$bitrate" -threads 8 -tile-columns 4 -frame-parallel 1 -speed 4 -g 128 -aq-mode 0 -an -sn -f webm /dev/null 
ffmpeg -i "$1" -c:v libvpx-vp9 -vf scale=-1:720 -pass 2 -qmin 0 -qmax 50 -crf 10 -b:v "$bitrate" -c:a libvorbis -b:a 96K -vbr on -threads 4 -tile-columns 4 -frame-parallel 1 -speed 1 -auto-alt-ref 1 -lag-in-frames 25 -g 128 -aq-mode 0 -sn -f webm "$2"
