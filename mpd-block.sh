#!/usr/bin/env bash
# from <https://github.com/vivien/i3blocks/wiki/Writing-Your-Own-Blocklet>

# Pass the password in the block instance
if [[ -n $BLOCK_INSTANCE ]]; then
    password=("-h" "$BLOCK_INSTANCE@localhost")
fi

format="[\\[[%composer%|%artist%]\\] %title%]|%file%"

filter() {
    #echo -n '['
    tr '\n' ' ' | grep -Po '.*(?= \[playing\])|paused' | tr -d '\n'
    #echo -n ']'
}

case $BLOCK_BUTTON in	# These are meant to be roughly acme's scrollbar conventions
    2) mpc -f "$format" toggle | filter ;;  # middle click, pause/unpause
    1) mpc -f "$format" prev   | filter ;;  # left click, previous
    3) mpc -f "$format" next   | filter ;;  # right click, next
    *) mpc -f "$format" status | filter ;;
esac
